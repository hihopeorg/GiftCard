package com.ldoblem.giftcardlib;



import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DragEvent;
import ohos.agp.components.ScrollHelper;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;

import ohos.agp.render.*;
import ohos.agp.text.RichText;
import ohos.agp.utils.Color;
import ohos.agp.utils.MimeData;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.app.dispatcher.TaskDispatcher;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import com.ldoblem.giftcardlib.models.Buyer;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

import java.io.IOException;
import java.util.Optional;

import static ohos.agp.components.AttrHelper.getDensity;

/**
 * Created by lumingmin on 16/7/14.
 */

public class GiftCardView extends Component implements Component.EstimateSizeListener, Component.DrawTask, RichText.TouchEventListener {
    private Paint mPaintBg;
    private Paint mPaintText;
    private Paint mPaintBuyButton;

    private Path mPathBg;
    private RectFloat rectFBg;
    private RectFloat rectFBgMove;

    private Path mPathPackLeft;
    private Path mPathPackTop;
    private Path mPathPackBottom;

    private Path mPathPackRight;

    private Path mPathsilkTop;
    private Path mPathsilkBottom;
    private Path mPathsilkLeft;
    private Path mPathsilkRight;

    private RectFloat rectFBuyButton;
    private RectFloat rectFCheckButton;

    private float mPadding = 0f;

    private int mCircular = 0;
    private Shader mShader;

    private String mTitle;

    private float mPrice = 25.00f;


    private String mButtonBuyText;
    private String mButtonCheckText;
    private String cardTip;


    private float mBuyButtonH = 0f;
    private float mBuyButtonW = 0f;

    private int bgStartColor = Color.rgb(231, 0, 148);
    private int bgEndColor = Color.rgb(164, 13, 158);
    private int buyButtonColor = Color.rgb(243, 152, 0);

    private int checkButtonColor = Color.rgb(8, 156, 239);

    private int cardBgColor = Color.WHITE.getValue();
    private Element giftLogo;

    private int bgPackBgColor = Color.rgb(252, 44, 44);

    private int mPriceTextColor = Color.rgb(150, 150, 150);

    private Buyer mBuyer;

    private OnCheckOut mOnCheckOut;

    private boolean pressBuyButton = false;
    private boolean pressCheckButton = false;

    private AnimatorValue valueAnimator;
    private float mAnimatedBgValue = 0f;
    private float mAmAnimatedPackValue = 0f;
    private PixelMapHolder pixelMapHolder;

    public GiftCardView(Context context) {
        this(context, null);
    }

    public GiftCardView(Context context, AttrSet attrs) {
        this(context, attrs, 0);
    }

    public GiftCardView(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
        initPaint();
        setEstimateSizeListener(this::onEstimateSize);
        addDrawTask(this::onDraw);
        setTouchEventListener(this::onTouchEvent);
    }

    public void setMTitle(String text) {
        this.mTitle = text;
    }

    public void setMPrice(float price) {
        this.mPrice = price;
    }

    public void setCardTip(String text) {
        this.cardTip = text;
    }

    public void setButtonBuyText(String text) {
        this.mButtonBuyText = text;
    }

    public void setButtonCheckText(String text) {
        this.mButtonCheckText = text;
    }

    public void setCardBgColor(int color) {
        this.cardBgColor = color;
    }

    public void setGiftLogo(Element id) {
        this.giftLogo = id;
    }

    public void setBgStartColor(int bgStartColor) {
        this.bgStartColor = bgStartColor;
    }

    public void setBgEndColor(int bgEndColor) {
        this.bgEndColor = bgEndColor;
    }

    public void setBuyButtonColor(int buyButtonColor) {
        this.buyButtonColor = buyButtonColor;
    }

    public void setCheckButtonColor(int checkButtonColor) {
        this.checkButtonColor = checkButtonColor;
    }

    public void setBgPackBgColor(int bgPackBgColor) {
        this.bgPackBgColor = bgPackBgColor;
    }

    public void setPriceTextColor(int mPriceTextColor) {
        this.mPriceTextColor = mPriceTextColor;
    }


    public void setOnCheckOut(Buyer buyer, OnCheckOut onCheckOut) {
        this.mBuyer = buyer;
        this.mOnCheckOut = onCheckOut;
        invalidate();
    }

    private void init(AttrSet attrs) {

        if (attrs != null) {
//            isCanvasLine = typedArray.getBoolean(R.styleable.SelectView_isCanvasLine, true);
//            mTextSize = typedArray.getDimension(R.styleable.SelectView_textSize, dip2px(12));
            if (attrs.getAttr("bgStartColor").isPresent()) {
                bgStartColor = attrs.getAttr("bgStartColor").get().getColorValue().getValue();
            }
            if (attrs.getAttr("bgEndColor").isPresent()) {
                bgEndColor = attrs.getAttr("bgEndColor").get().getColorValue().getValue();
            }

            if (attrs.getAttr("buyButtonColor").isPresent()) {
                buyButtonColor = attrs.getAttr("buyButtonColor").get().getColorValue().getValue();
            }

            if (attrs.getAttr("checkButtonColor").isPresent()) {
                checkButtonColor = attrs.getAttr("checkButtonColor").get().getColorValue().getValue();
            }

            if (attrs.getAttr("cardBgColor").isPresent()) {
                cardBgColor = attrs.getAttr("cardBgColor").get().getColorValue().getValue();
            }

            if (attrs.getAttr("bgPackColor").isPresent()) {
                bgPackBgColor = attrs.getAttr("bgPackColor").get().getColorValue().getValue();
            }

            if (attrs.getAttr("priceTextColor").isPresent()) {
                mPriceTextColor = attrs.getAttr("priceTextColor").get().getColorValue().getValue();
            }

            try {
                if (attrs.getAttr("cardGiftLogo").isPresent()) {
                    giftLogo = attrs.getAttr("cardGiftLogo").get().getElement();
                } else {
                    giftLogo = new PixelMapElement(getPixelMap());
                }
            } catch (Exception e) {
                e.printStackTrace();
                String message = e.getMessage();
            }

            if (attrs.getAttr("cardGiftTitle").isPresent()) {
                mTitle = attrs.getAttr("cardGiftTitle").get().getStringValue();
            } else if (mTitle == null) {
                mTitle = getContext().getString(ResourceTable.String_card_title);
            }

            cardTip = getContext().getString(ResourceTable.String_card_tip);

            if (attrs.getAttr("buttonCheckText").isPresent()) {
                mButtonCheckText = attrs.getAttr("buttonCheckText").get().getStringValue();
            } else if (mButtonCheckText == null) {
                mButtonCheckText = getContext().getString(ResourceTable.String_button_check_test);
            }

            if (attrs.getAttr("buttonByText").isPresent()) {
                mButtonBuyText = attrs.getAttr("buttonByText").get().getStringValue();
            } else if (mButtonBuyText == null) {
                mButtonBuyText = getContext().getString(ResourceTable.String_button_buy);
            }

            if (attrs.getAttr("cardGiftPrice").isPresent()) {
                mPrice = attrs.getAttr("cardGiftPrice").get().getFloatValue();
            }
        }



    }

    private PixelMap getPixelMap() {
        PixelMap pixelMap = null;
        try {
            Resource bgResource = getResourceManager().getResource(ResourceTable.Media_apple);
            ImageSource.SourceOptions srcOpts = new ImageSource.SourceOptions();
            srcOpts.formatHint = "image/png";
            ImageSource imageSource = ImageSource.create(bgResource, srcOpts);
            // 设置图片参数
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            pixelMap = imageSource.createPixelmap(decodingOptions);
        } catch (IOException | NotExistException e) {
            e.printStackTrace();
        }
        return pixelMap;
    }


    public void setBuyer(Buyer buyer) {
        this.mBuyer = buyer;
    }

    private void initPaint() {

        mBuyer = new Buyer("ldoublem", "Zhejiang Province",
                "Hangzhou , Xihu , Nanshan Road", "Available to ship: 1 business day");

        mPaintBg = new Paint();
        mPaintBg.setAntiAlias(true);
        mPaintBg.setColor(Color.WHITE);
        mPaintBg.setStyle(Paint.Style.FILL_STYLE);

        mPaintText = new Paint();
        mPaintText.setAntiAlias(true);
        mPaintText.setColor(Color.WHITE);
        mPaintText.setStyle(Paint.Style.FILL_STYLE);


        mPaintBuyButton = new Paint();
        mPaintBuyButton.setAntiAlias(true);
        mPaintBuyButton.setColor(new Color(buyButtonColor));
        mPaintBuyButton.setStyle(Paint.Style.FILL_STYLE);


        mPathBg = new Path();
        mPathBg.reset();
        mPathPackLeft = new Path();
        mPathPackBottom = new Path();
        mPathPackTop = new Path();
        mPathPackRight = new Path();
        mPathPackLeft.reset();
        mPathPackRight.reset();
        mPathPackTop.reset();
        mPathPackBottom.reset();


        mPathsilkTop = new Path();
        mPathsilkBottom = new Path();
        mPathsilkRight = new Path();
        mPathsilkLeft = new Path();
        mPathsilkTop.reset();
        mPathsilkBottom.reset();
        mPathsilkLeft.reset();
        mPathsilkRight.reset();
        rectFBg = new RectFloat();
        rectFBgMove = new RectFloat();
        rectFBuyButton = new RectFloat();
        rectFCheckButton = new RectFloat();
        mPadding = vp2Px(mContext, 1);

    }


    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        int widthSpecMode = EstimateSpec.getMode(widthMeasureSpec);
        int widthSpecSize = EstimateSpec.getSize(widthMeasureSpec);
        int heightSpecMode = EstimateSpec.getMode(heightMeasureSpec);
        int heightSpecSize = EstimateSpec.getSize(heightMeasureSpec);
        if (widthSpecMode == EstimateSpec.PRECISE
                && heightSpecMode == EstimateSpec.PRECISE) {
            // 指定wrap_content模式（MeasureSpec.AT_MOST）的默认宽高，比如200px
            setEstimatedSize(
                    EstimateSpec.getChildSizeWithMode(vp2Px(mContext, 200), vp2Px(mContext, 200), EstimateSpec.PRECISE),
                    EstimateSpec.getChildSizeWithMode(vp2Px(mContext, 150), vp2Px(mContext, 150), EstimateSpec.PRECISE));
        } else if (widthSpecMode == EstimateSpec.PRECISE) {
            setEstimatedSize(
                    EstimateSpec.getChildSizeWithMode(vp2Px(mContext, 200), vp2Px(mContext, 200), EstimateSpec.PRECISE),
                    EstimateSpec.getChildSizeWithMode(heightSpecSize, heightSpecSize, EstimateSpec.PRECISE));
        } else if (heightSpecMode == EstimateSpec.PRECISE) {
            setEstimatedSize(
                    EstimateSpec.getChildSizeWithMode(widthSpecSize, widthSpecSize, EstimateSpec.PRECISE),
                    EstimateSpec.getChildSizeWithMode(vp2Px(mContext, 150), vp2Px(mContext, 150), EstimateSpec.PRECISE));
        }

        return true;
    }

    private void drawBg(Canvas canvas) {
        mPathBg.reset();
        mPathBg.moveTo(rectFBg.left, rectFBg.top + rectFBg.getHeight() / 3f * 2f);
        mPathBg.lineTo(rectFBg.left, rectFBg.top + mCircular);
        mPathBg.quadTo(rectFBg.left, rectFBg.top, rectFBg.left + mCircular, rectFBg.top);
        mPathBg.lineTo(rectFBg.right - mCircular, rectFBg.top);
        mPathBg.quadTo(rectFBg.right, rectFBg.top, rectFBg.right, rectFBg.top + mCircular);
        mPathBg.lineTo(rectFBg.right, rectFBg.top + rectFBg.getHeight() / 3f * 2f);
        mPathBg.close();

        mPaintBg.setColor(new Color(cardBgColor));
        canvas.drawPath(mPathBg, mPaintBg);

        mPaintText.setTextSize(mCircular - 1);

        mBuyButtonW = (getFontHeight(mPaintText, mButtonBuyText)) * 4.5f;

        mBuyButtonH = mBuyButtonW / 3f;

        rectFBgMove.top = rectFBg.top;
        rectFBgMove.bottom = rectFBg.bottom;
        rectFBgMove.left = rectFBg.left
                - mAnimatedBgValue * (rectFBg.getWidth() - mCircular * 2 - mBuyButtonW);
        rectFBgMove.right = rectFBg.right
                - mAnimatedBgValue * (rectFBg.getWidth() - mCircular * 2 - mBuyButtonW);
    }

    private void drawCardTopBg(Canvas canvas) {
        mPathBg.reset();

        float mRightLeft = mCircular * mAnimatedBgValue;
        float mTop = mAnimatedBgValue * rectFBg.getHeight() / 3f * 2f;

        if (rectFBg.getHeight() / 3f * 2f - mTop < mCircular / 2) {
            if (mAnimatedBgValue <= 1.0f) {
                mPathBg.moveTo(rectFBg.left + mRightLeft,
                        rectFBg.top + rectFBg.getHeight() / 3f * 2f);
                mPathBg.lineTo(rectFBg.left + mRightLeft,
                        rectFBg.top + rectFBg.getHeight() / 3f * 2f - 2);
                mPathBg.lineTo(rectFBg.right - mRightLeft,
                        rectFBg.top + rectFBg.getHeight() / 3f * 2f - 2);
                mPathBg.lineTo(rectFBg.right - mRightLeft,
                        rectFBg.top + rectFBg.getHeight() / 3f * 2f);
            } else {
                mRightLeft = mCircular;
                mPathBg.moveTo(rectFBg.left + mRightLeft,
                        rectFBg.top + rectFBg.getHeight() / 3f * 2f);
                mPathBg.lineTo(rectFBg.left + mRightLeft,
                        rectFBg.top + rectFBg.getHeight() / 3f * 2f - 2);
                mPathBg.lineTo(rectFBg.right - mRightLeft,
                        rectFBg.top + rectFBg.getHeight() / 3f * 2f - 2);
                mPathBg.lineTo(rectFBg.right - mRightLeft,
                        rectFBg.top + rectFBg.getHeight() / 3f * 2f);
            }

        } else {
            mPathBg.moveTo(rectFBg.left + mRightLeft, rectFBg.top + rectFBg.getHeight() / 3f * 2f);
            mPathBg.lineTo(rectFBg.left + mRightLeft, rectFBg.top + mCircular + mTop);
            mPathBg.quadTo(rectFBg.left + mRightLeft, rectFBg.top + mTop,
                    rectFBg.left + mCircular + mRightLeft, rectFBg.top + mTop);
            mPathBg.lineTo(rectFBg.right - mCircular - mRightLeft, rectFBg.top + mTop);
            mPathBg.quadTo(rectFBg.right - mRightLeft, rectFBg.top + mTop,
                    rectFBg.right - mRightLeft, rectFBg.top + mCircular + mTop);
            mPathBg.lineTo(rectFBg.right - mRightLeft, rectFBg.top + rectFBg.getHeight() / 3f * 2f);
            mPathBg.close();

        }
        setShader(mPaintBg, bgStartColor, bgEndColor);
        canvas.drawPath(mPathBg, mPaintBg);
    }

    private void drawCardTopBgShadow(Canvas canvas) {
        if (mAnimatedBgValue > 0.1f) {
            return;
        }
        mPathBg.reset();

        mPathBg.moveTo(rectFBg.left, rectFBg.top + rectFBg.getHeight() / 3f * 2f);
        mPathBg.lineTo(rectFBg.left, rectFBg.top + rectFBg.getHeight() / 3f * 2f
                - (rectFBg.getHeight() / 3f * 2f) / 4f);
        mPathBg.quadTo(rectFBg.left,
                rectFBg.top + rectFBg.getHeight() / 3f * 2f,
                rectFBg.right,
                rectFBg.top + rectFBg.getHeight() / 3f
        );
        mPathBg.lineTo(rectFBg.right, rectFBg.top + rectFBg.getHeight() / 3f * 2f);
        mPathBg.close();

        mPaintBg.setColor(new Color(Color.argb(30, 255, 255, 255)));
        canvas.drawPath(mPathBg, mPaintBg);
    }

    private void drawLogo(Canvas canvas) {
        if (mAnimatedBgValue < 1f) {
            mPaintBg.setColor(Color.WHITE);
            PixelMapHolder ios = setBitmapSize(giftLogo, (int) (rectFBg.getHeight() / 4f));
          /*  canvas.drawBitmap(ios, rectFBg.centerX() - ios.getWidth() / 2,
                    rectFBg.top + rectFBg.getHeight() / 3f - ios.getHeight() / 2
                            + rectFBg.getHeight() / 4.5f * (mAnimatedBgValue), mPaintBg);*/
            canvas.drawPixelMapHolder(ios, rectFBg.getHorizontalCenter() - ios.getPixelMap().getImageInfo().size.width / 2,
                    rectFBg.top + rectFBg.getHeight() / 3f - ios.getPixelMap().getImageInfo().size.height / 2
                            + rectFBg.getHeight() / 4.5f * (mAnimatedBgValue), mPaintBg);
        }
    }

    private void drawCardBottomBg(Canvas canvas) {
        mPathBg.reset();
        mPathBg.moveTo(rectFBg.left, rectFBg.top + rectFBg.getHeight() / 3f * 2f);
        mPathBg.lineTo(rectFBg.left, rectFBg.bottom - mCircular);
        mPathBg.quadTo(rectFBg.left, rectFBg.bottom,
                rectFBg.left + mCircular, rectFBg.bottom);
        mPathBg.lineTo(rectFBg.right - mCircular, rectFBg.bottom);
        mPathBg.quadTo(rectFBg.right, rectFBg.bottom,
                rectFBg.right, rectFBg.bottom - mCircular);
        mPathBg.lineTo(rectFBg.right, rectFBg.top + rectFBg.getHeight() / 3f * 2f);
        mPathBg.close();

        mPaintBg.setColor(new Color(cardBgColor));
        canvas.drawPath(mPathBg, mPaintBg);
    }

    private void drawTitleAndPrice(Canvas canvas) {
        setShader(mPaintText, bgStartColor, bgEndColor);
        mPaintText.setTextSize((int) (mCircular + 1));
        canvas.drawText(mPaintText, mTitle, rectFBgMove.left + mCircular,
                rectFBgMove.top + rectFBgMove.getHeight() / 3f * 2f + rectFBgMove.getHeight() / 6f
                        - getFontHeight(mPaintText, mTitle) / 2f);
//        mPaintText.setShader();

        mPaintText.setColor(new Color(mPriceTextColor));
        mPaintText.setTextSize((int) (mCircular - 1));

        String price = "$" + new java.text.DecimalFormat("#.00").format(mPrice);

        canvas.drawText(mPaintText, price, rectFBgMove.left + mCircular,
                rectFBgMove.top + rectFBgMove.getHeight() / 3f * 2f
                        + rectFBgMove.getHeight() / 6f + getFontHeight(mPaintText, price));
    }

    private void drawBuyButton(Canvas canvas) {
        mPaintBuyButton.setColor(new Color(buyButtonColor));
        mPaintText.setTextSize((int) (mCircular - 1));
        rectFBuyButton.top = rectFBgMove.top + rectFBgMove.getHeight() / 3f * 2f
                + rectFBgMove.getHeight() / 6f - mBuyButtonH;
        rectFBuyButton.bottom = rectFBgMove.top + rectFBgMove.getHeight() / 3f * 2f
                + rectFBgMove.getHeight() / 6f + mBuyButtonH;
        rectFBuyButton.right = rectFBgMove.right - mCircular;
        rectFBuyButton.left = rectFBgMove.right - mCircular - mBuyButtonW;

        if (pressBuyButton) {
            rectFBuyButton.top += 4;
            rectFBuyButton.left += 4;
            rectFBuyButton.bottom -= 4;
            rectFBuyButton.right -= 4;
        }

        canvas.drawRoundRect(rectFBuyButton, mCircular / 2f, mCircular / 2f, mPaintBuyButton);
        mPaintText.setColor(Color.WHITE);
        canvas.drawText(mPaintText, mButtonBuyText,
                rectFBuyButton.getHorizontalCenter() - getFontlength(mPaintText, mButtonBuyText) / 2f,
                rectFBuyButton.getVerticalCenter() + getFontHeight(mPaintText, mButtonBuyText) / 3f);

        if (mAmAnimatedPackValue > 0f && mAmAnimatedPackValue <= 0.4f) {
            drawBuyButtonFrom0to4(canvas);
        } else if (mAmAnimatedPackValue > 0.4f && mAmAnimatedPackValue <= 0.5f) {
            drawBuyButtonFrom4to5(canvas);
        } else if (mAmAnimatedPackValue > 0.5f && mAmAnimatedPackValue <= 1f) {
            drawBuyButtonFrom5to10(canvas);
        }
    }

    private void drawBuyButtonFrom0to4(Canvas canvas) {
        float packValueFirst = mAmAnimatedPackValue / 0.4f;
        float maxHigh = rectFBuyButton.getHeight() / 2f * 0.8f;

        mPaintBuyButton.setColor(new Color(bgPackBgColor));
        mPathPackTop.reset();
        mPathPackTop.moveTo(rectFBuyButton.left + mCircular / 2f, rectFBuyButton.top);
        mPathPackTop.lineTo(rectFBuyButton.right - mCircular / 2f, rectFBuyButton.top);

        mPathPackTop.lineTo(rectFBuyButton.right - mCircular / 2f
                - maxHigh * (1 - packValueFirst), rectFBuyButton.top
                - maxHigh * (1 - packValueFirst));

        mPathPackTop.lineTo(rectFBuyButton.left + mCircular / 2f
                        + maxHigh * (1 - packValueFirst),
                rectFBuyButton.top - maxHigh * (1 - packValueFirst));

        mPathPackTop.close();
        mPaintBuyButton.setAlpha(200);
        canvas.drawPath(mPathPackTop, mPaintBuyButton);

        mPathPackTop.reset();
        mPathPackTop.moveTo(rectFBuyButton.right - mCircular / 2f
                        - maxHigh * (1 - packValueFirst),
                rectFBuyButton.top - maxHigh * (1 - packValueFirst));

        mPathPackTop.lineTo(rectFBuyButton.left + mCircular / 2f
                        + maxHigh * (1 - packValueFirst),
                rectFBuyButton.top - maxHigh * (1 - packValueFirst));

        mPathPackTop.lineTo(rectFBuyButton.getHorizontalCenter(),
                rectFBuyButton.top - maxHigh * (1 - packValueFirst) + maxHigh * packValueFirst);
        mPathPackTop.close();
        mPaintBuyButton.setAlpha(255);

        canvas.drawPath(mPathPackTop, mPaintBuyButton);

        mPathPackBottom.reset();
        mPathPackBottom.moveTo(rectFBuyButton.left + mCircular / 2f, rectFBuyButton.bottom);
        mPathPackBottom.lineTo(rectFBuyButton.right - mCircular / 2f, rectFBuyButton.bottom);

        mPathPackBottom.lineTo(rectFBuyButton.right - mCircular / 2f
                        - maxHigh * (1 - packValueFirst),
                rectFBuyButton.bottom + maxHigh * (1 - packValueFirst));

        mPathPackBottom.lineTo(rectFBuyButton.left + mCircular / 2f
                        + maxHigh * (1 - packValueFirst),
                rectFBuyButton.bottom + maxHigh * (1 - packValueFirst));

        mPathPackBottom.close();
        mPaintBuyButton.setAlpha(200);
        canvas.drawPath(mPathPackBottom, mPaintBuyButton);

        mPathPackBottom.reset();
        mPathPackBottom.moveTo(rectFBuyButton.right - mCircular / 2f
                        - maxHigh * (1 - packValueFirst),
                rectFBuyButton.bottom + maxHigh * (1 - packValueFirst));

        mPathPackBottom.lineTo(rectFBuyButton.left + mCircular / 2f
                        + maxHigh * (1 - packValueFirst),
                rectFBuyButton.bottom + maxHigh * (1 - packValueFirst));
        mPathPackBottom.lineTo(rectFBuyButton.getHorizontalCenter(),
                rectFBuyButton.bottom + maxHigh * (1 - packValueFirst)
                        - maxHigh * packValueFirst

        );
        mPathPackBottom.close();
        mPaintBuyButton.setAlpha(255);

        canvas.drawPath(mPathPackBottom, mPaintBuyButton);

        float maxWidth = rectFBuyButton.getHeight() / 2f * 0.5f;

        mPathPackLeft.reset();
        mPathPackLeft.moveTo(rectFBuyButton.left, rectFBuyButton.top + mCircular / 2f);
        mPathPackLeft.lineTo(rectFBuyButton.left, rectFBuyButton.bottom - mCircular / 2f);
        mPathPackLeft.lineTo(rectFBuyButton.left - maxWidth * (1 - packValueFirst),
                rectFBuyButton.bottom - mCircular / 2f - maxWidth * (1 - packValueFirst)
        );
        mPathPackLeft.lineTo(rectFBuyButton.left - maxWidth * (1 - packValueFirst),
                rectFBuyButton.top + mCircular / 2f + maxWidth * (1 - packValueFirst));

        mPathPackLeft.close();
        mPaintBuyButton.setAlpha(200);
        canvas.drawPath(mPathPackLeft, mPaintBuyButton);

        mPathPackLeft.reset();
        mPathPackLeft.moveTo(rectFBuyButton.left - maxWidth * (1 - packValueFirst),
                rectFBuyButton.bottom - mCircular / 2f - maxWidth * (1 - packValueFirst));
        mPathPackLeft.lineTo(rectFBuyButton.left - maxWidth * (1 - packValueFirst),
                rectFBuyButton.top + mCircular / 2f + maxWidth * (1 - packValueFirst));

        mPathPackLeft.lineTo(rectFBuyButton.left - maxWidth * (1 - packValueFirst)
                + maxWidth * packValueFirst, rectFBuyButton.getVerticalCenter());

        mPathPackLeft.close();
        mPaintBuyButton.setAlpha(255);
        canvas.drawPath(mPathPackLeft, mPaintBuyButton);

        mPathPackRight.reset();
        mPathPackRight.moveTo(rectFBuyButton.right, rectFBuyButton.top + mCircular / 2f);
        mPathPackRight.lineTo(rectFBuyButton.right, rectFBuyButton.bottom - mCircular / 2f);
        mPathPackRight.lineTo(rectFBuyButton.right + maxWidth * (1 - packValueFirst),
                rectFBuyButton.bottom - mCircular / 2f - maxWidth * (1 - packValueFirst)
        );
        mPathPackRight.lineTo(rectFBuyButton.right + maxWidth * (1 - packValueFirst),
                rectFBuyButton.top + mCircular / 2f + maxWidth * (1 - packValueFirst));

        mPathPackRight.close();
        mPaintBuyButton.setAlpha(200);
        canvas.drawPath(mPathPackRight, mPaintBuyButton);

        mPathPackRight.reset();
        mPathPackRight.moveTo(rectFBuyButton.right + maxWidth * (1 - packValueFirst),
                rectFBuyButton.bottom - mCircular / 2f - maxWidth * (1 - packValueFirst));
        mPathPackRight.lineTo(rectFBuyButton.right + maxWidth * (1 - packValueFirst),
                rectFBuyButton.top + mCircular / 2f + maxWidth * (1 - packValueFirst));

        mPathPackRight.lineTo(rectFBuyButton.right + maxWidth * (1 - packValueFirst)
                        - maxWidth * packValueFirst,
                rectFBuyButton.getVerticalCenter());

        mPathPackRight.close();
        mPaintBuyButton.setAlpha(255);
        canvas.drawPath(mPathPackRight, mPaintBuyButton);
    }

    private void drawBuyButtonFrom4to5(Canvas canvas) {
        mPaintBuyButton.setColor(new Color(bgPackBgColor));

        canvas.drawRoundRect(rectFBuyButton, mCircular / 2f, mCircular / 2f, mPaintBuyButton);

        float packValueFirst = (mAmAnimatedPackValue - 0.4f) / 0.1f;

        mPaintBuyButton.setColor(new Color(Color.rgb(253, 209, 48)));
        //            mPaintBuyButton.setAlpha(200);
        mPathsilkTop.reset();
        mPathsilkTop.moveTo(rectFBuyButton.left + mCircular * 1.1f, rectFBuyButton.top);
        mPathsilkTop.lineTo(rectFBuyButton.left + mCircular * 1.1f + mCircular / 1.5f,
                rectFBuyButton.top);
        mPathsilkTop.lineTo(rectFBuyButton.left + mCircular * 1.1f + mCircular / 1.5f,
                rectFBuyButton.top - packValueFirst * mCircular / 2f);
        mPathsilkTop.lineTo(rectFBuyButton.left + mCircular * 1.1f,
                rectFBuyButton.top - packValueFirst * mCircular / 2f
        );

        mPathsilkTop.close();
        canvas.drawPath(mPathsilkTop, mPaintBuyButton);

        mPathsilkBottom.reset();
        mPathsilkBottom.moveTo(rectFBuyButton.left + mCircular * 1.1f, rectFBuyButton.bottom);
        mPathsilkBottom.lineTo(rectFBuyButton.left + mCircular * 1.1f + mCircular / 1.5f,
                rectFBuyButton.bottom);
        mPathsilkBottom.lineTo(rectFBuyButton.left + mCircular * 1.1f + mCircular / 1.5f,
                rectFBuyButton.bottom + mCircular / 2f * packValueFirst);
        mPathsilkBottom.lineTo(rectFBuyButton.left + mCircular * 1.1f, rectFBuyButton.bottom
                + mCircular / 2f * packValueFirst
        );

        mPathsilkBottom.close();
        canvas.drawPath(mPathsilkBottom, mPaintBuyButton);

        mPathsilkLeft.reset();
        mPathsilkLeft.moveTo(rectFBuyButton.left, rectFBuyButton.top + mCircular / 1.4f);
        mPathsilkLeft.lineTo(rectFBuyButton.left, rectFBuyButton.top + mCircular / 1.4f
                + mCircular / 1.5f);
        mPathsilkLeft.lineTo(rectFBuyButton.left - mCircular / 2f * packValueFirst
                , rectFBuyButton.top + mCircular / 1.4f + mCircular / 1.5f
        );
        mPathsilkLeft.lineTo(rectFBuyButton.left - mCircular / 2f * packValueFirst
                , rectFBuyButton.top + mCircular / 1.4f
        );

        mPathsilkLeft.close();
        canvas.drawPath(mPathsilkLeft, mPaintBuyButton);

        mPathsilkRight.reset();
        mPathsilkRight.moveTo(rectFBuyButton.right, rectFBuyButton.top + mCircular / 1.4f);
        mPathsilkRight.lineTo(rectFBuyButton.right, rectFBuyButton.top + mCircular / 1.4f
                + mCircular / 1.5f);
        mPathsilkRight.lineTo(rectFBuyButton.right + mCircular / 2f * packValueFirst,
                rectFBuyButton.top + mCircular / 1.4f + mCircular / 1.5f
        );
        mPathsilkRight.lineTo(rectFBuyButton.right + mCircular / 2f * packValueFirst,
                rectFBuyButton.top + mCircular / 1.4f
        );

        mPathsilkRight.close();
        canvas.drawPath(mPathsilkRight, mPaintBuyButton);
    }

    private void drawBuyButtonFrom5to10(Canvas canvas) {
        mPaintBuyButton.setColor(new Color(bgPackBgColor));

        canvas.drawRoundRect(rectFBuyButton, mCircular / 2f, mCircular / 2f, mPaintBuyButton);

        float packValueSecond = (mAmAnimatedPackValue - 0.5f) / 0.5f;
        if (packValueSecond >= 1f) {
            packValueSecond = 1f;
        }

        mPaintBuyButton.setColor(new Color(Color.rgb(253, 209, 48)));
        float maxSilkTopH = rectFBuyButton.getHeight() / 3f + mCircular;
        float maxSilkBottpmH = rectFBuyButton.getHeight() / 3f * 2 + mCircular;
        mPathsilkTop.reset();
        mPathsilkTop.moveTo(rectFBuyButton.left + mCircular * 1.1f, rectFBuyButton.top);
        mPathsilkTop.lineTo(rectFBuyButton.left + mCircular * 1.1f + mCircular / 1.5f,
                rectFBuyButton.top);
        mPathsilkTop.lineTo(rectFBuyButton.left + mCircular * 1.1f + mCircular / 1.5f,
                rectFBuyButton.top + maxSilkTopH * packValueSecond - mCircular / 2f
        );
        mPathsilkTop.lineTo(rectFBuyButton.left + mCircular * 1.1f,
                rectFBuyButton.top + maxSilkTopH * packValueSecond - mCircular / 2f
        );

        mPathsilkTop.close();
        canvas.drawPath(mPathsilkTop, mPaintBuyButton);

        mPathsilkBottom.reset();
        mPathsilkBottom.moveTo(rectFBuyButton.left + mCircular * 1.1f, rectFBuyButton.bottom);
        mPathsilkBottom.lineTo(rectFBuyButton.left + mCircular * 1.1f + mCircular / 1.5f,
                rectFBuyButton.bottom);
        mPathsilkBottom.lineTo(rectFBuyButton.left + mCircular * 1.1f + mCircular / 1.5f,
                rectFBuyButton.bottom - maxSilkBottpmH * packValueSecond + mCircular / 2f);
        mPathsilkBottom.lineTo(rectFBuyButton.left + mCircular * 1.1f,
                rectFBuyButton.bottom - maxSilkBottpmH * packValueSecond + mCircular / 2f
        );

        mPathsilkBottom.close();
        canvas.drawPath(mPathsilkBottom, mPaintBuyButton);

        float maxSilkLeftW = mCircular + mCircular;

        mPathsilkLeft.reset();
        mPathsilkLeft.moveTo(rectFBuyButton.left, rectFBuyButton.top + mCircular / 1.4f);
        mPathsilkLeft.lineTo(rectFBuyButton.left, rectFBuyButton.top + mCircular / 1.4f
                + mCircular / 1.5f);
        mPathsilkLeft.lineTo(rectFBuyButton.left + maxSilkLeftW * packValueSecond
                        - mCircular / 2f,
                rectFBuyButton.top + mCircular / 1.4f + mCircular / 1.5f
        );
        mPathsilkLeft.lineTo(rectFBuyButton.left + maxSilkLeftW * packValueSecond
                        - mCircular / 2f,
                rectFBuyButton.top + mCircular / 1.4f
        );

        mPathsilkLeft.close();
        canvas.drawPath(mPathsilkLeft, mPaintBuyButton);

        float maxSilkRightW = rectFBuyButton.getWidth() - mCircular + mCircular;

        mPathsilkRight.reset();
        mPathsilkRight.moveTo(rectFBuyButton.right, rectFBuyButton.top + mCircular / 1.4f);
        mPathsilkRight.lineTo(rectFBuyButton.right, rectFBuyButton.top + mCircular / 1.4f
                + mCircular / 1.5f);
        mPathsilkRight.lineTo(rectFBuyButton.right - maxSilkRightW * packValueSecond
                        + mCircular / 2f,
                rectFBuyButton.top + mCircular / 1.4f + mCircular / 1.5f
        );
        mPathsilkRight.lineTo(rectFBuyButton.right - maxSilkRightW * packValueSecond
                        + mCircular / 2f,
                rectFBuyButton.top + mCircular / 1.4f
        );

        mPathsilkRight.close();
        canvas.drawPath(mPathsilkRight, mPaintBuyButton);

        if (mAmAnimatedPackValue > 0.8f && mAmAnimatedPackValue <= 1f) {
            mPaintBuyButton.setColor(new Color(Color.rgb(254, 230, 51)));
            float packValueThird = (mAmAnimatedPackValue - 0.8f) / 0.2f;

            Path bowknotLeftTop = new Path();
            bowknotLeftTop.reset();

            float x = (float) ((mCircular / 3f) * Math.cos(90 * Math.PI / 180f));
            float y = (float) ((mCircular / 3f) * Math.sin(90 * Math.PI / 180f));

            bowknotLeftTop.moveTo(rectFBuyButton.left + mCircular * 1.1f + mCircular / 3f - x,
                    rectFBuyButton.top + mCircular / 1.4f + mCircular / 3f - y);

            float x2 = (float) ((mCircular / 3f) * Math.cos(-60 * Math.PI / 180f));
            float y2 = (float) ((mCircular / 3f) * Math.sin(-60 * Math.PI / 180f));

            bowknotLeftTop.lineTo(rectFBuyButton.left + mCircular * 1.1f + mCircular / 3f - x2,
                    rectFBuyButton.top + mCircular / 1.4f + mCircular / 3f - y2);

            bowknotLeftTop.lineTo(rectFBuyButton.left + mCircular * 1.1f
                            + mCircular / 3f - x2 - mCircular,
                    rectFBuyButton.top + mCircular / 1.4f - y2);

            bowknotLeftTop.lineTo(rectFBuyButton.left + mCircular * 1.1f
                            + mCircular / 3f - x - mCircular,
                    rectFBuyButton.top + mCircular / 1.4f - y);
            bowknotLeftTop.close();
            canvas.drawPath(bowknotLeftTop, mPaintBuyButton);

            Path bowknotRighrTop = new Path();
            bowknotRighrTop.reset();
            float x3 = (float) ((mCircular / 3f) * Math.cos(240 * Math.PI / 180f));
            float y3 = (float) ((mCircular / 3f) * Math.sin(240 * Math.PI / 180f));

            bowknotRighrTop.moveTo(rectFBuyButton.left + mCircular * 1.1f + mCircular / 3f - x,
                    rectFBuyButton.top + mCircular / 1.4f + mCircular / 3f - y);

            bowknotRighrTop.lineTo(rectFBuyButton.left + mCircular * 1.1f + mCircular / 3f - x3,
                    rectFBuyButton.top + mCircular / 1.4f + mCircular / 3f - y3);
            bowknotRighrTop.lineTo(rectFBuyButton.left + mCircular * 1.1f
                            + mCircular / 3f - x3 + mCircular,
                    rectFBuyButton.top + mCircular / 1.4f - y3);

            bowknotRighrTop.lineTo(rectFBuyButton.left + mCircular * 1.1f
                            + mCircular / 3f - x + mCircular,
                    rectFBuyButton.top + mCircular / 1.4f - y);

            bowknotRighrTop.close();
            canvas.drawPath(bowknotRighrTop, mPaintBuyButton);

            float space = mCircular / 3 * 2 * 1.2f;
            Path bowknotLeftBottom = new Path();
            bowknotLeftBottom.reset();
            float x4 = (float) ((mCircular / 3f) * Math.cos(270 * Math.PI / 180f));
            float y4 = (float) ((mCircular / 3f) * Math.sin(270 * Math.PI / 180f));
            bowknotLeftBottom.moveTo(rectFBuyButton.left + mCircular * 1.1f
                            + mCircular / 3f - x4,
                    rectFBuyButton.top + mCircular / 1.4f + mCircular / 3f - y4);

            bowknotLeftBottom.lineTo(rectFBuyButton.left + mCircular * 1.1f
                            + mCircular / 3f - x4 - space,
                    rectFBuyButton.top + mCircular / 1.4f + mCircular / 3f - y4 + space);

            bowknotLeftBottom.lineTo(rectFBuyButton.left + mCircular * 1.1f
                            + mCircular / 3f - x4 - space,
                    rectFBuyButton.top + mCircular / 1.4f + mCircular / 3f - y4
                            + space - space / 2);

            bowknotLeftBottom.lineTo(rectFBuyButton.left + mCircular * 1.1f
                            + mCircular / 3f - x4 - space - space / 2,
                    rectFBuyButton.top + mCircular / 1.4f + mCircular / 3f - y4
                            + space - space / 2);

            bowknotLeftBottom.lineTo(rectFBuyButton.left + mCircular * 1.1f
                            + mCircular / 3f - x4,
                    rectFBuyButton.top + mCircular / 1.4f + mCircular / 3f - y4 - space);
            bowknotLeftBottom.close();
            canvas.drawPath(bowknotLeftBottom, mPaintBuyButton);

            Path bowknotRightBottom = new Path();
            bowknotRightBottom.reset();

            bowknotRightBottom.moveTo(rectFBuyButton.left + mCircular * 1.1f
                            + mCircular / 3f - x4,
                    rectFBuyButton.top + mCircular / 1.4f + mCircular / 3f - y4);

            bowknotRightBottom.lineTo(rectFBuyButton.left + mCircular * 1.1f
                            + mCircular / 3f - x4 + space,
                    rectFBuyButton.top + mCircular / 1.4f + mCircular / 3f - y4 + space);

            bowknotRightBottom.lineTo(rectFBuyButton.left + mCircular * 1.1f
                            + mCircular / 3f - x4 + space,
                    rectFBuyButton.top + mCircular / 1.4f + mCircular / 3f - y4
                            + space - space / 2);

            bowknotRightBottom.lineTo(rectFBuyButton.left + mCircular * 1.1f
                            + mCircular / 3f - x4 + space + space / 2,
                    rectFBuyButton.top + mCircular / 1.4f + mCircular / 3f - y4
                            + space - space / 2);

            bowknotRightBottom.lineTo(rectFBuyButton.left + mCircular * 1.1f
                            + mCircular / 3f - x4,
                    rectFBuyButton.top + mCircular / 1.4f + mCircular / 3f - y4 - space);
            bowknotRightBottom.close();
            canvas.drawPath(bowknotRightBottom, mPaintBuyButton);

            mPaintBuyButton.setColor(Color.GRAY);

            canvas.drawCircle(rectFBuyButton.left + mCircular * 1.1f + mCircular / 3f,
                    rectFBuyButton.top + mCircular / 1.4f + mCircular / 3f,
                    (mCircular / 3f * 1.401f) * packValueThird, mPaintBuyButton);

            mPaintBuyButton.setColor(new Color(Color.rgb(254, 230, 51)));

            canvas.drawCircle(rectFBuyButton.left + mCircular * 1.1f + mCircular / 3f,
                    rectFBuyButton.top + mCircular / 1.4f + mCircular / 3f,
                    (mCircular / 3f * 1.4f) * packValueThird,
                    mPaintBuyButton
            );
        }
    }

    private void drawCheckOutButton(Canvas canvas) {

        if (mAnimatedBgValue > 0.5f) {
            mPaintBuyButton.setColor(new Color(checkButtonColor));
            rectFCheckButton.top = rectFBgMove.top + rectFBgMove.getHeight() / 3f * 2f
                    + rectFBgMove.getHeight() / 6f - mBuyButtonH;
            rectFCheckButton.bottom = rectFBgMove.top + rectFBgMove.getHeight() / 3f * 2f
                    + rectFBgMove.getHeight() / 6f + mBuyButtonH;
            rectFCheckButton.right = rectFBgMove.right - mCircular
                    + (rectFBg.getWidth() - mCircular * 2 - mBuyButtonW);
            rectFCheckButton.left = rectFBgMove.right - mCircular - mBuyButtonW
                    + (rectFBg.getWidth() - mCircular * 2 - mBuyButtonW);


            if (pressCheckButton) {
                rectFCheckButton.bottom -= 4;
                rectFCheckButton.top += 4;
                rectFCheckButton.left += 4;
                rectFCheckButton.right -= 4;
            }

            canvas.drawRoundRect(rectFCheckButton, mCircular / 2f, mCircular / 2f, mPaintBuyButton);
            mPaintText.setColor(Color.WHITE);
            mPaintText.setTextSize((int) mCircular - 1);
            canvas.drawText(mPaintText, mButtonCheckText, rectFCheckButton.getHorizontalCenter() - getFontlength(mPaintText,
                    mButtonCheckText) / 2f,
                    rectFCheckButton.getVerticalCenter() + getFontHeight(mPaintText, mButtonCheckText) / 3f);
        }
    }

    private void drawCardContent(Canvas canvas) {

        if (mAnimatedBgValue > 0.6f) {
            mPaintText.setTextSize((int) (mCircular * 0.8f));
            mPaintText.setColor(Color.BLACK);
            canvas.drawText(mPaintText, cardTip, rectFBuyButton.left,
                    rectFBg.top + mCircular + getFontHeight(mPaintText, cardTip));


            if (mBuyer != null) {
                canvas.drawText(mPaintText, mBuyer.getName(), rectFBuyButton.left,
                        rectFBg.top + mCircular + getFontHeight(mPaintText, mBuyer.getName()) * 3);

                mPaintText.setColor(new Color(mPriceTextColor));

                canvas.drawText(mPaintText, mBuyer.getRegion(), rectFBuyButton.left,
                        rectFBg.top + mCircular + getFontHeight(mPaintText, mBuyer.getRegion()) * 4
                                + getFontHeight(mPaintText, mBuyer.getName()) / 2);
                canvas.drawText(mPaintText, mBuyer.getAddress(), rectFBuyButton.left,
                        rectFBg.top + mCircular + getFontHeight(mPaintText, mBuyer.getAddress()) * 5f
                                + getFontHeight(mPaintText, mBuyer.getName()) / 2);

                canvas.drawText(mPaintText, mBuyer.getAvailableDay(), rectFBuyButton.left,
                        rectFBg.top + mCircular + getFontHeight(mPaintText,
                                mBuyer.getAvailableDay()) * 8);
            }
        }
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        canvas.save();
        mCircular = (getEstimatedWidth()/*getMeasuredWidth()*/ / 20);
        rectFBg.top = mPadding;
        rectFBg.left = mPadding;
        rectFBg.bottom = getEstimatedHeight() - mPadding;
        rectFBg.right = getEstimatedWidth() - mPadding;

        drawBg(canvas);
        drawCardTopBg(canvas);
        drawCardTopBgShadow(canvas);
        drawLogo(canvas);
        drawCardBottomBg(canvas);
        drawTitleAndPrice(canvas);
        drawBuyButton(canvas);
        drawCheckOutButton(canvas);
        drawCardContent(canvas);
        canvas.restore();
    }


    private float getFontlength(Paint paint, String str) {
//        Rect rect = new Rect();
//        paint.getTextBounds(str, 0, str.length(), rect);
        ohos.agp.utils.Rect textBounds = paint.getTextBounds(str);
        return textBounds.getWidth();
    }

    private float getFontHeight(Paint paint, String str) {
//        Rect rect = new Rect();
        ohos.agp.utils.Rect textBounds = paint.getTextBounds(str);
        return textBounds.getHeight();
    }


    public static int vp2Px(Context context, float dpValue) {
        return (int) (dpValue * getDensity(context) + 0.5f);
    }

    public static int px2vp(Context context, int pxValue) {
        Optional<Display> defaultDisplay = DisplayManager.getInstance().getDefaultDisplay(context);
        int densityDpi = defaultDisplay.map(display -> display.getRealAttributes().densityDpi).orElse(1);
        return densityDpi * pxValue;
    }


    /*private Bitmap setBitmapSize(int iconId, int w) {
        Bitmap bitmap = BitmapFactory.decodeResource(getContext().getResources(), iconId);
        float s = w * 1.0f / bitmap.getWidth();
        bitmap = Bitmap.createScaledBitmap(bitmap, (int) (bitmap.getWidth() * s),
                (int) (bitmap.getHeight() * s), true);
        return bitmap;
    }*/

    private PixelMapHolder setBitmapSize(Element iconId, int w) {

        try {
            PixelMapElement element = (PixelMapElement) iconId;
            PixelMap pixelMap = element.getPixelMap();

            float s = w * 1.0f / pixelMap.getImageInfo().size.width;

            PixelMap.InitializationOptions ops = new PixelMap.InitializationOptions();
            ops.size = new Size(
                    (int) (pixelMap.getImageInfo().size.width * s),
                    (int) (pixelMap.getImageInfo().size.height * s));

            PixelMap map = PixelMap.create(pixelMap, ops);

            PixelMapHolder holder = new PixelMapHolder(map);
            return holder;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }


//        ImageSource bitmap;
//        PixelMap pixelMapreturn = null;
//        try {
//            bitmap = ImageSource.create(getContext().getResourceManager().getResource(iconId), null);
//
//            float s = w * 1.0f / bitmap.getImageInfo().size.width;
//            ImageSource.DecodingOptions decodingOpts = new ImageSource.DecodingOptions();
////            decodingOpts.desiredRegion = new Rect(0, 0, 0, 0);
//            PixelMap pixelmap = bitmap.createPixelmap(decodingOpts);
//            PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
//            initializationOptions.size = new Size((int) (bitmap.getImageInfo().size.width * s), (int) (bitmap.getImageInfo().size.height * s));
//            pixelMapreturn = PixelMap.create(pixelmap, decodingOpts.desiredRegion, initializationOptions);//TODO：如果为空，说明创建不成功
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (NotExistException e) {
//            e.printStackTrace();
//        }
//        pixelMapHolder = new PixelMapHolder(pixelMapreturn);
//        return pixelMapHolder;
    }

    private void setShader(Paint p, int startColor, int endColor) {
        Point pivot = new RectFloat(rectFBg.left, rectFBg.top, rectFBg.right, rectFBg.bottom).getPivot();
        Point[] points = {pivot};
        mShader = new LinearShader(points, new float[]{0f, 1f}, new Color[]{new Color(startColor), new Color(endColor)}, Shader.TileMode.CLAMP_TILEMODE);
        p.setColor(new Color(startColor));
        p.setShader(mShader, Paint.ShaderType.LINEAR_SHADER);
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        int action = event.getAction();
        MmiPoint mmiPoint = event.getPointerPosition(0);
        DragEvent obtain = DragEvent.obtain(action, mmiPoint.getX(), mmiPoint.getY(), new MimeData());

        switch (action) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                pressBuyButton = rectFBuyButton.isInclude(obtain.getX(), obtain.getY()) && mAmAnimatedPackValue == 0f;
                invalidate();
                break;
            case TouchEvent.HOVER_POINTER_MOVE:
                if (rectFBuyButton.isInclude/*contains*/(obtain.getX(), obtain.getY())
                        && mAmAnimatedPackValue == 0f) {
                    pressBuyButton = true;
                    invalidate();
                } else if (pressBuyButton) {
                    pressBuyButton = false;
                    invalidate();
                }

                if (rectFCheckButton.isInclude(obtain.getX(), obtain.getY())
                        && mAnimatedBgValue == 1.0f) {
                    pressCheckButton = true;
                    invalidate();
                } else if (pressCheckButton) {
                    pressCheckButton = false;
                    invalidate();
                }


                break;
            case TouchEvent.PRIMARY_POINT_UP:
            case TouchEvent.CANCEL:
                pressBuyButton = false;
                pressCheckButton = false;
                if (rectFBuyButton.isInclude(obtain.getX(), obtain.getY())
                        && mAmAnimatedPackValue == 0f) {
                    startAnim(1);
                } else if (rectFCheckButton.isInclude(obtain.getX(), obtain.getY())
                        && mAnimatedBgValue == 1.0f) {
                    if (mOnCheckOut != null) {
                        mOnCheckOut.ok(getId());
                    } else {
                        myToast(1000, "OnCheckOut is null");
                    }

                    TaskDispatcher dispatcher = mContext.getUITaskDispatcher();
                    dispatcher.delayDispatch(() -> {
                        invalidate();
                    }, 0);

                }
                return false;
            default:
                break;
        }
        return true;
    }

    private void myToast(int i, String mText) {
        new ToastDialog(mContext).setDuration(i).setText(mText).show();
    }

    private void startAnim(int step) {
        if (step == 1) {
            stopAnim();
            startViewAnim(0f, 1f, 600, step);
        } else if (step == 2) {
            startViewAnim(0f, 1f, 400, step);

        }
    }

    public ScrollHelper getScrollHelper() {//新增
        ScrollHelper instance = new ScrollHelper();
        return instance;
    }

    public void restore() {
        if (valueAnimator != null) {
            getScrollHelper().abortAnimation();
            valueAnimator.setLoopedCount(0);
            valueAnimator.cancel();
            mAmAnimatedPackValue = 0f;
            mAnimatedBgValue = 0f;
            TaskDispatcher dispatcher = mContext.getUITaskDispatcher();
            dispatcher.delayDispatch(() -> {
                invalidate();
            }, 0);
        }
    }

    private void stopAnim() {
        if (valueAnimator != null) {
            getScrollHelper().abortAnimation();
            valueAnimator.setLoopedCount(0);
            valueAnimator.cancel();
//            valueAnimator.end();
            mAmAnimatedPackValue = 0f;
            mAnimatedBgValue = 0f;
            TaskDispatcher dispatcher = mContext.getUITaskDispatcher();
            dispatcher.delayDispatch(() -> {
                invalidate();
            }, 0);

        }
    }


    private AnimatorValue startViewAnim(float startF, final float endF, long time, final int step) {
        valueAnimator = new AnimatorValue();
        valueAnimator.setDuration(time);
        AnimatorGroup animatorGroup = new AnimatorGroup();
        animatorGroup.runParallel();
//        valueAnimator.setLoopedListener(null);
        valueAnimator.setLoopedCount(0);//重复次数是0，
        valueAnimator.setLoopedListener(Animator::start);
        valueAnimator.setValueUpdateListener((animatorValue, v) -> {
            if (step == 1) {
                    mAmAnimatedPackValue = v;
            } else if (step == 2) {
                    mAnimatedBgValue = v;

            }
                invalidate();
        });

        valueAnimator.setStateChangedListener(new Animator.StateChangedListener() {

            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                if (step == 1) {
                    startAnim(2);
                }
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }

        });

        if (!valueAnimator.isRunning()) {
            valueAnimator.start();
        }
        return valueAnimator;
    }


    public interface OnCheckOut {
        void ok(int vid);
    }


}
