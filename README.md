# GiftCard

**本项目是基于开源项目GiftCard进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/ldoublem/GiftCard ）追踪到原项目版本**

#### 项目介绍

- 项目名称：GiftCard
- 所属系列：ohos的第三方组件适配移植
- 功能：可以自定义控件的炫酷卡片控件，支持自定义多种属性
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/ldoublem/GiftCard 
- 原项目基线版本: 无release版本, sha1:71decad75b2b8db27787b64050273e34dbbfb61e
- 编程语言：Java
- 外部库依赖：无

#### 效果演示

![](./giftCard.gif)

#### 安装教程

方法一：

1. 编译GiftCard的har包giftcardlib.har。

2. 启动 DevEco Studio，将编译的har包，导入工程目录“entry->libs”下。

3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

   ```
   dependencies {
       implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
       ……
   }
   ```
   
4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

方法二：

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址:

```
 repositories {
     maven {
         url 'http://106.15.92.248:8081/repository/Releases/'
     }
 }
```

2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
 dependencies {
     implementation 'com.ldoublem.giftcard:GiftCard:1.0.1'
 }
```

#### 使用说明
1 .xml中使用
```
 <com.ldoblem.giftcardlib.GiftCardView
                ohos:height="200vp"
                ohos:width="300vp"
                card:bgPackColor="#56abe4"
                card:bgStartColor="#11cd6e"
                card:buttonByText="购买"
                card:buttonCheckText="确定"
                card:buyButtonColor="#11cd6e"
                card:cardBgColor="#33475f"
                card:cardGiftTitle="礼物卡"
                card:checkButtonColor="#2c2c2c"
                card:priceTextColor="#fff"
                />
```
2. 代码中使用
```
        mGiftCardView = (GiftCardView) findComponentById(ResourceTable.Id_gc_shop);
        mGiftCardView.setMTitle("苹果礼券");
        mGiftCardView.setMPrice(188);
        mGiftCardView.setButtonBuyText("买");
        mGiftCardView.setButtonCheckText("确定");
        mGiftCardView.setCardTip("请检查你的购物单");
        mGiftCardView.setCardBgColor(Color.BLACK);
        mGiftCardView.setGiftLogo(R.mipmap.ic_launcher);
        mGiftCardView.setBgStartColor(Color.BLACK);
        mGiftCardView.setBgEndColor(Color.BLACK);
        mGiftCardView.setBuyButtonColor(Color.BLACK);
        mGiftCardView.setCheckButtonColor(Color.BLACK);
        mGiftCardView.setPriceTextColor(Color.BLACK);
        mGiftCardView.setBgPackBgColor(Color.BLACK);
        mGiftCardView.setOnCheckOut(new GiftCardView.Buyer("陆先生", "中国浙江省",
                        "杭州市,西湖区,南山路100号", "有效期:3天"),
                new GiftCardView.OnCheckOut() {
                    @Override
                    public void Ok(int vid) {
                        
                    }
                });
```

#### 版本迭代

- v1.0.1

#### 版权和许可信息
```
The MIT License (MIT)

Copyright (c) 2016 ldoublem

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```