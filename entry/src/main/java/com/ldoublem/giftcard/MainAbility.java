package com.ldoublem.giftcard;

import com.ldoblem.giftcardlib.GiftCardView;
import com.ldoblem.giftcardlib.models.Buyer;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class MainAbility extends Ability implements GiftCardView.OnCheckOut {
    GiftCardView mGiftCardView;

    @Override
    public void onStart(Intent intent) {
        super.setUIContent(ResourceTable.Layout_activity_main);
        mGiftCardView = (GiftCardView) findComponentById(ResourceTable.Id_gc_shop);
        mGiftCardView.setOnCheckOut(new Buyer("陆先生", "中国浙江省",
                "杭州市,西湖区,南山路100号", "有效期:3天"),this);
    }

    @Override
    public void ok(int vid) {
        if (vid==ResourceTable.Id_gc_shop){
            mGiftCardView.restore();
        }
    }
}
