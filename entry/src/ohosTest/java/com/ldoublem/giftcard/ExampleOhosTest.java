/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ldoublem.giftcard;

import com.ldoblem.giftcardlib.GiftCardView;
import com.ldoublem.giftcard.utile.EventHelper;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.animation.AnimatorValue;
import ohos.eventhandler.EventHandler;
import ohos.multimodalinput.event.TouchEvent;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;

public class ExampleOhosTest {
    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.ldoublem.giftcard", actualBundleName);
    }
    @Test
    public void testStart() {
        MainAbility ability = EventHelper.startAbility(MainAbility.class);
        gotoSleep(3000);
        long downTime = System.currentTimeMillis();
        long eventTime = downTime;
        TouchEvent down = EventHelper.obtainTouchEvent(downTime, eventTime, EventHelper.ACTION_DOWN, 737, 1844);
        sAbilityDelegator.triggerTouchEvent(ability, down);
        TouchEvent up = EventHelper.obtainTouchEvent(downTime, eventTime, EventHelper.ACTION_UP, 737, 1844);
        sAbilityDelegator.triggerTouchEvent(ability, up);
        gotoSleep(3000);
        Class<GiftCardView> gifobj = GiftCardView.class;
        try {
            GiftCardView o = gifobj.newInstance();
            Method startViewAnim = gifobj.getMethod("startViewAnim");
            AnimatorValue animVaule = (AnimatorValue) startViewAnim.invoke(o);
            Assert.assertNotNull(animVaule);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private void gotoSleep(long l) {
        try {
            Thread.sleep(l);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}